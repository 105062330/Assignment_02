# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report 
*  **A complete game process: start menu => game view => game over => quit or play again             [V]**
    * 我的遊戲過程: menu -> input name -> choose role -> game view -> game over -> quit or play agin
    
* **Your game should follow the basic rules of  "小朋友下樓梯". [V]**
    * 玩家不停的往下走，有台階可以踏，地圖會一直往上，上面有尖刺，掉下去的話會死, 速度也會越來越快.地形有台階, 假的台階, 尖刺(損血，走台階可以補血), 彈簧, 往左往右的滾輪.

* **All things in your game should have correct physical properties and behaviors.[V]**
    * 每個玩家和平台都有碰撞機制, 玩家也有往下的重力.

* **Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) [V]**
    * 遊戲中有5種平台, 有正常的可以補血, 有假的平台, 有尖刺, 有彈簧還有往右往左的滾輪.

* **Add some additional sound effects and UI to enrich your game. [V]**
    * 玩家被尖刺撞到的話會有emitter跑出來, 死掉後視窗會有震動的效果, 並且碰到刺, 跳到彈簧或著gameover都會有不同的音效, 而字體也會有掉下來, 旋轉和放大的特效.

* **Store player's name and score in firebase real-time database, and add a leaderboard to your game.  [V]**
    * 在menu可以選選擇計分板, 而計分板會有前八名的名字和階層, 計分板還利用css animation達到bling的效果.

* **Other creative features in your game. [V]**
    * 我在記分板用了css animation和bling的效果
    * 我的遊戲可以選擇角色, 共有三種角色可以選擇
    * 除了基本的one player之外還有two player可以遊玩