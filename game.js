// Initialize Phaser
var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');// Define our global variable
var input;
var score;
var userInfoText="";
var bool=-1;
var char=0;
var player3_x;
game.global = { score: 0 };
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('input', inputState);
game.state.add('character', charState);
game.state.add('play', playState);
game.state.add('play2', play2State);

// Start the 'boot' state
game.state.start('boot');