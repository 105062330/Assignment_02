

var playState = {
    preload: function(){

    },
    create: function(){
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.hitSound = game.add.audio('hit');
        this.jumpSound = game.add.audio('jump');
        this.screamSound = game.add.audio('scream');
        this.selectSound = game.add.audio('select');
        this.screamSound.volume=1;


        //var player;
        //var keyboard;

        this.platforms = [];

        //var leftWall;
        //var rightWall;
        //var ceiling;

        //var text1;
        //var text2;
        //var text3;

        this.distance = 0;
        this.last_distance=0;
        this.status = 'running';
        this.lastTime = 0;
        this.lastTime1 = 0;

        this.keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'q': Phaser.Keyboard.Q,
        });
    
        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
        //this.createOnePlatform();
    },
    update: function(){
        starfield.tilePosition.y -= 2;

        // bad
        if(this.status == 'gameOver' && this.keyboard.enter.isDown) this.restart();
        if(this.status == 'gameOver' && this.keyboard.q.isDown) this.quit();
        if(this.status != 'running') return;
    
        game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
        game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
        this.checkTouchCeiling(this.player);
        this.checkGameOver();
    
        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();
    
        this.createPlatforms();
    },
    createBounders: function(){
        this.leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;
    
        this.rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;
    
        this.ceiling = game.add.image(0, 0, 'ceiling');
    },
    createPlatforms: function(){
        if(game.time.now > this.lastTime + 1200) {
            this.lastTime = game.time.now;
            
            this.distance += 1;
        } else if(game.time.now > this.lastTime1 + 700){
            this.createOnePlatform();
            this.lastTime1 = game.time.now;
        }
    },
    createOnePlatform: function(){
        var platform;
        var x = Math.random()*(400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;
    
        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
    
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);
    
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },
    createPlayer: function(){
        if(char==1){
            this.player = game.add.sprite(200, 50, 'player');
            game.physics.arcade.enable(this.player);
            this.player.body.gravity.y = 500;
            this.player.animations.add('left', [0, 1, 2, 3], 8);
            this.player.animations.add('right', [9, 10, 11, 12], 8);
            this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
            this.player.animations.add('flyright', [27, 28, 29, 30], 12);
            this.player.animations.add('fly', [36, 37, 38, 39], 12);
            this.player.life = 10;
            this.player.unbeatableTime = 0;
            this.player.touchOn = undefined;
        } else if(char==2){
            this.player = game.add.sprite(200, 50, 'player2');
            game.physics.arcade.enable(this.player);
            this.player.body.gravity.y = 500;
            this.player.animations.add('left', [0, 1, 2, 3], 8);
            this.player.animations.add('right', [5, 6, 7, 8], 8);
            this.player.animations.add('flyleft', [0, 1, 2, 3], 12);
            this.player.animations.add('flyright', [5, 6, 7, 8], 12);
            this.player.animations.add('fly', [4], 12);
            this.player.life = 10;
            this.player.unbeatableTime = 0;
            this.player.touchOn = undefined;
        } else if(char==3){
            this.player = game.add.sprite(200, 50, 'player3');
            game.physics.arcade.enable(this.player);
            this.player.body.gravity.y = 500;
            this.player.animations.add('left', [4, 5, 6, 7], 8);
            this.player.animations.add('right', [0, 1, 2, 3], 8);
            this.player.animations.add('flyleft', [4, 5, 6, 7], 12);
            this.player.animations.add('flyright', [0, 1, 2, 3], 12);
            this.player.life = 10;
            this.player.unbeatableTime = 0;
            this.player.touchOn = undefined;      
        }
    },
    createTextsBoard: function(){
        var style = {fill: '#ffffff', font: '20px Arial'}
        this.text1 = game.add.text(20, 15, '', style);
        this.text2 = game.add.text(355, 15, '', style);
        this.text3 = game.add.text(game.width/2, -50, 'Game Over', style);
        this.text4 = game.add.text(game.width/2, 340, 'Press Enter To Continue',
        { font: '20px Arial', fill: '#ffffff' }); 
        this.text5 = game.add.text(game.width/2, 370, 'Press Q To quit',
        { font: '20px Arial', fill: '#ffffff' }); 
        this.text3.anchor.setTo(0.5, 0.5);
        this.text4.anchor.setTo(0.5, 0.5);
        this.text5.anchor.setTo(0.5, 0.5);
        this.text3.visible = false;
        this.text4.visible=false;
        this.text5.visible=false;

       
    },
    updatePlayer: function(){
        if(this.keyboard.left.isDown) {
            this.player.body.velocity.x = -250;
        } else if(this.keyboard.right.isDown) {
            this.player.body.velocity.x = 250;
        } else {
            this.player.body.velocity.x = 0;
        }
        this.setPlayerAnimate(this.player);
    },
    setPlayerAnimate: function(player){
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
    
        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
            player3_x='left';
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
            player3_x='right';
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
            player3_x='left';
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
            player3_x='right';
        }
        if (x == 0 && y != 0) {
            if(char==3){
                if(player3_x=='left')
                    player.frame = 4;
                else    
                    player.frame = 0;
            }
            else
                player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            if(char==1)
                player.frame = 8;
            else if(char==2)
                player.animations.play('fly');
            else if(char==3){
                if(player3_x=='left')
                    player.frame = 4;
                else    
                    player.frame = 0;
            }
        }
    },  
    updatePlatforms: function(){
        for(var i=0; i<this.platforms.length; i++) {
            var platform = this.platforms[i];
            if(this.distance<=30)
                platform.body.position.y -= 2;
            else if(this.distance>30 && this.distance<=60)
                platform.body.position.y -= 2.5;
            else if(this.distance>60 && this.distance<=100)
                platform.body.position.y -= 3.5;      
            else if(this.distance>100 && this.distance<=200)
                platform.body.position.y -= 5;
            else
                platform.body.position.y -= 7;

            if(platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },
    updateTextsBoard: function(){
        this.text1.setText('life:' + this.player.life);
        this.text2.setText('B' + this.distance);
    },
    effect: function(player, platform){
        if(platform.key == 'conveyorRight') {
            this.conveyorRightEffect(this.player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            this.conveyorLeftEffect(this.player, platform);
        }
        if(platform.key == 'trampoline') {
            this.trampolineEffect(this.player, platform);
        }
        if(platform.key == 'nails') {
            this.nailsEffect(this.player, platform);
        }
        if(platform.key == 'normal') {
            this.basicEffect(this.player, platform);
        }
        if(platform.key == 'fake') {
            this.fakeEffect(this.player, platform);
        }
    },
    conveyorRightEffect: function(player, platform){
        this.player.body.x += 2;
    },
    conveyorLeftEffect: function(player, platform){
        this.player.body.x -= 2;
    },
    trampolineEffect: function(player, platform){
        this.jumpSound.play();
        platform.animations.play('jump');
        this.player.body.velocity.y = -350;
    },
    nailsEffect: function(player, platform){
        if (this.player.touchOn !== platform) {
            this.emitter = game.add.emitter(this.player.x, this.player.y, 20);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150); 
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.start(true, 800, null, 20);
            this.hitSound.play();
            this.player.life -= 3;
            if(this.player.life<0)this.player.life=0;
            this.player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
        }
    },
    basicEffect: function(player, platform){
        if (this.player.touchOn !== platform) {
            if(this.player.life < 10) {
                this.player.life += 1;
            }
            this.player.touchOn = platform;
        }
    },
    fakeEffect: function(player, platform){
        if(this.player.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            this.player.touchOn = platform;
        }
    },
    checkTouchCeiling: function(player){
        if(this.player.body.y < 0) {
            if(this.player.body.velocity.y < 0) {
                this.player.body.velocity.y = 0;
            }
            if(game.time.now > this.player.unbeatableTime) {
                this.emitter = game.add.emitter(this.player.x, this.player.y, 20);
                this.emitter.makeParticles('pixel');
                this.emitter.setYSpeed(-150, 150); 
                this.emitter.setXSpeed(-150, 150);
                this.emitter.setScale(2, 0, 2, 0, 800);
                this.emitter.gravity = 500;
                this.emitter.start(true, 800, null, 20);

                this.hitSound.play();

                this.player.life -= 3;
                if(this.player.life<0)
                    this.player.life=0;

                game.camera.flash(0xff0000, 100);
                this.player.unbeatableTime = game.time.now + 2000;
            }
        }
        
    },
    checkGameOver: function(){
        if(this.player.body.y > 500 || this.player.life<=0) {
            this.screamSound.play();
            game.camera.shake(0.02, 300);
        }
            
        if(this.player.life <= 0 || this.player.body.y > 500) {
            this.gameOver();
        }
    },
    gameOver: function(){

        this.text3.visible = true;
        game.add.tween(this.text3).to({y: 200}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        this.text4.visible=true;
        this.text5.visible=true;
        game.add.tween(this.text1.scale).to({x: 1.5, y: 1.5}, 200).yoyo(true).start();
        game.add.tween(this.text2.scale).to({x: 1.5, y: 1.5}, 200).yoyo(true).start();
        game.add.tween(this.text4.scale).to({x: 1.5, y: 1.5}, 400).yoyo(true).start();
        game.add.tween(this.text5.scale).to({x: 1.5, y: 1.5}, 400).yoyo(true).start();
        this.platforms.forEach(function(s) {s.destroy()});
        this.platforms = [];
        this.status = 'gameOver';
        score=this.distance;
        this.writedata();
    },
    restart: function(){
        this.selectSound.play();
        this.screamSound.volume=0;
        game.state.start('character');
        this.text3.visible = false;
        this.text4.visible=false;
        this.text5.visible=false;
        this.distance = 0;
        //this.createPlayer();
        this.status = 'running';
    },
    writedata: function(){
        firebase.database().ref('users').push({
            name: input.value,
            score: this.distance,
          }).catch(function(error){
            console.error("寫入使用者資訊錯誤",error);
          });   
        if(game.global.score==0) 
            game.global.score=this.distance;
        else{
            if(this.distance>=game.global.score)
                game.global.score=this.distance;
        }
    },
    quit: function(){
        this.selectSound.play();
        game.state.start('menu');
    }
};
