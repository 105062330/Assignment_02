var bootState = { 
    preload: function () {
    // Load the progress bar image.
    game.load.image('progressBar', 'assets/CustomProgressBar.png'); 
    },
    create: function() {
    // Set some game settings. 
    //game.stage.backgroundColor = '#3498db'; 
    game.stage.backgroundColor = '#ffffff'; 
    game.physics.startSystem(Phaser.Physics.ARCADE); 
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
    } 
};