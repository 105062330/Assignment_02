var charState={
    create: function(){
        game.add.image(0, 0, 'starfield');
        var clickSound = game.add.audio('click');

        var nameLabel = game.add.text(game.width/2, -50, 'Choose your role',
        { font: '40px Arial', fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();


        var player = game.add.sprite(100, game.height/2+5, 'player');
        var player2 = game.add.sprite(190, game.height/2-10, 'player2');
        var player3 = game.add.sprite(280, game.height/2, 'player3');


        player.animations.add('right', [9, 10, 11, 12], 8, true);
        player2.animations.add('right', [5, 6, 7, 8], 8, true);
        player3.animations.add('right', [0, 1, 2, 3], 8, true);
        

        player.animations.play('right');
        player.inputEnabled = true;
        player.input.useHandCursor = true;
        player.events.onInputDown.add(function() {   
            clickSound.play();
            char=1;
            game.state.start('play');
        });

        player2.animations.play('right');
        player2.inputEnabled = true;
        player2.input.useHandCursor = true;
        player2.events.onInputDown.add(function() {   
            clickSound.play();
            char=2;
            game.state.start('play');
        });

        player3.animations.play('right');
        player3.inputEnabled = true;
        player3.input.useHandCursor = true;
        player3.events.onInputDown.add(function() {   
            clickSound.play();
            char=3;
            game.state.start('play');
        });
    
    },
    start: function(){
        game.state.start('play');
    },
}