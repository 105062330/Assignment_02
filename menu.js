var menuState = { 
    create: function() {

    this.selectSound = game.add.audio('select');
    // Add a background image 
    //game.add.image(0, 0, 'background');
    game.add.image(0, 0, 'starfield');
    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, -50, 'NS Shaft',
    { font: '50px Arial', fill: '#ffffff' }); 
    nameLabel.anchor.setTo(0.5, 0.5);
    //var tween = game.add.tween(nameLabel);
    //tween.to({y: 80}, 1000);
    //tween.easing(Phaser.Easing.Bounce.Out);
    //tween.start();
    game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    // Show the score at the center of the screen

    //var scoreLabel = game.add.text(game.width/2, game.height/2,
    //'leader B' + game.global.score, { font: '25px Arial', fill: '#ffffff' }); 
    //scoreLabel.anchor.setTo(0.5, 0.5);

    var ranking = game.add.image(200, 200, 'rankings');
    ranking.anchor.setTo(0.5, 0.5);
    ranking.inputEnabled = true;
    ranking.input.useHandCursor = true;
    ranking.events.onInputDown.add(function() {
        window.location="firebase-leaderboard.html";
    });


    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-100,
    'the up arrow key for single', { font: '25px Arial', fill: '#ffffff' }); 
    startLabel.anchor.setTo(0.5, 0.5);
    game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    //game.add.tween(startLabel.scale).to({x: 1.5, y: 1.5}, 300).yoyo(true).start();

    var startLabel1 = game.add.text(game.width/2, game.height-60,
        'the down arrow key for 2p', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel1.anchor.setTo(0.5, 0.5);
    game.add.tween(startLabel1).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    //game.add.tween(startLabel1.scale).to({x: 1.5, y: 1.5}, 300).yoyo(true).start();

    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this); 

    var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    downKey.onDown.add(this.start2, this);


    },
    start: function() {
        this.selectSound.play();
        // Start the actual game 
        game.state.start('input');

    }, 
    start2: function(){
        this.selectSound.play();
        game.state.start('play2');
    },
};