var loadState = { 
    preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(40, 100, 'progressBar'); 
    //progressBar.anchor.setTo(0.5, 0.5); 
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    //game.load.image('player', 'assets/player.png'); 
    //game.load.image('enemy', 'assets/enemy.png'); 
    //game.load.image('coin', 'assets/coin.png'); 
    //game.load.image('wallV', 'assets/wallVertical.png'); 
    //game.load.image('wallH', 'assets/wallHorizontal.png');

    game.load.spritesheet('player', './assets/player.png', 32, 32);
    game.load.spritesheet('player2', './assets/phaser.png', 32, 48);
    game.load.spritesheet('player3', './assets/hero.png', 26.7, 42);
    game.load.image('normal', './assets/normal.png');
    game.load.image('nails', './assets/nails.png');
    game.load.image('starfield', 'assets/starfield.png');
    game.load.image('bg', 'assets/bg.png');
    game.load.spritesheet('conveyorRight', './assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', './assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', './assets/fake.png', 96, 36);
    game.load.image('wall', './assets/wall.png');
    game.load.image('ceiling', './assets/ceiling.png');
    game.load.audio('hit', 'assets/sounds/hit.wav');
    game.load.audio('jump', 'assets/sounds/jump.ogg');
    game.load.audio('scream', 'assets/sounds/scream.wav');
    game.load.audio('select', 'assets/sounds/button.wav');
    game.load.audio('click', 'assets/sounds/button.wav');
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('rankings', 'assets/250-512.png');
    game.load.spritesheet('btn', './assets/button-anim.png', 40.4, 41);
    // Load a new asset that we will use in the menu state
    //game.load.image('background', 'assets/background.png'); 
    },
    create: function() {
        // Go to the menu state 
        game.state.start('menu');
    } 
};